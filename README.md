# Let's dive into Hive! #

During the [Hortonworks Apache Spark certification](https://hortonworks.com/services/training/certification/hdp-certified-spark-developer/), we're going to be 
certainly asked to load some data from HDFS and save them into [Apache HIVE](https://hive.apache.org/)...Well, there are many ways of howto do that...Let's start
with obvious way:

Sample of **Input data** (movies.txt):

**Task**

Load the input data into **permanent** Hive table HDFSMoviesPart[x].

    [root@sandbox ~]# cat movies.txt
    1,scifi,Matrix,USA
    2,scifi,Matrix-Reloaded,USA
    3,comedy,TheBigBangTheory,USA
    4,comedy,MrBean,GBR
    5,comedy,CrocodileDundee,AUS
    6,comedy,Pelisky,CZE
    7,crime,CSI-NewYork,USA

## Solution 1. RDD -> Dataframe (case class way) -> Hive permanent table ##
(launched from Apache Zeppelin...I love this tool!)

    %spark

    import org.apache.spark.sql.hive.HiveContext;

    var hiveContext = new HiveContext(sc);

    case class Movie(id: Int, genre: String, name: String, country: String);

    val fileRDD = sc.textFile("/tests/movies.txt");

    val movieDF = fileRDD.map(line=>new Movie(line.split(",")(0).toInt, line.split(",")(1), line.split(",")(2), line.split(",")(3))).toDF();

    movieDF.write.mode("overwrite").saveAsTable("HDFSMoviesPart1");

    movieDF.show();

    hiveContext.sql("SHOW TABLES").show();

Output:

    import org.apache.spark.sql.hive.HiveContext
    hiveContext: org.apache.spark.sql.hive.HiveContext = org.apache.spark.sql.hive.HiveContext@73453df6
    defined class Movie
    fileRDD: org.apache.spark.rdd.RDD[String] = /tests/movies.txt MapPartitionsRDD[28] at textFile at <console>:44
    movieDF: org.apache.spark.sql.DataFrame = [id: int, genre: string, name: string, country: string]
    +---+------+----------------+-------+
    | id| genre|            name|country|
    +---+------+----------------+-------+
    |  1| scifi|          Matrix|    USA|
    |  2| scifi| Matrix-Reloaded|    USA|
    |  3|comedy|TheBigBangTheory|    USA|
    |  4|comedy|          MrBean|    GBR|
    |  5|comedy| CrocodileDundee|    AUS|
    |  6|comedy|         Pelisky|    CZE|
    |  7| crime|     CSI-NewYork|    USA|
    +---+------+----------------+-------+
    +---------------+-----------+
    |      tableName|isTemporary|
    +---------------+-----------+
    |        courses|      false|
    |          data2|      false|
    |          data3|      false|
    |           empl|      false|
    |          empl2|      false|
    |      employees|      false|
    |hdfsmoviespart1|      false|
    |         movies|      false|
    |      permtable|      false|
    |     permtable1|      false|
    |      sample_07|      false|
    |      sample_08|      false|
    |          stuff|      false|
    +---------------+-----------+

This code is pretty straightforward and it copies my previous samples. I will add just few notes:  

- This way makes sense only when you want to filter some input data.
- When you want to do just HDFS -> Hive 1:1 load then **this is a lot of code**...And time is strongly against you on HDP Apache Spark Developer Certification...:-)

## Solution 2. HIVE EXTERNAL TABLE ##

Another solution is to create [Hive External table](http://dwgeek.com/hive-create-external-tables-examples.html/) right onto existing HDFS data.
First, let's put movies.txt input file into HDFS /tests/dir directory.

    %spark

    import org.apache.spark.sql.hive.HiveContext;

    var hiveContext = new HiveContext(sc);

    hiveContext.sql("CREATE EXTERNAL TABLE IF NOT EXISTS HDFSMoviesPart2 (id int, genre string, name string, country string) "+
                " ROW FORMAT DELIMITED "+
                " FIELDS TERMINATED BY ','"+
                " LOCATION '/tests/dir'"
    );

    hiveContext.sql("SELECT * from HDFSMoviesPart2").show();

    hiveContext.sql("SHOW TABLES").show();

Output from Zeppelin run:

    import org.apache.spark.sql.hive.HiveContext
    hiveContext: org.apache.spark.sql.hive.HiveContext = org.apache.spark.sql.hive.HiveContext@3eef9af0
    res14: org.apache.spark.sql.DataFrame = [result: string]
    +---+------+----------------+-------+
    | id| genre|            name|country|
    +---+------+----------------+-------+
    |  1| scifi|          Matrix|    USA|
    |  2| scifi| Matrix-Reloaded|    USA|
    |  3|comedy|TheBigBangTheory|    USA|
    |  4|comedy|          MrBean|    GBR|
    |  5|comedy| CrocodileDundee|    AUS|
    |  6|comedy|         Pelisky|    CZE|
    |  7| crime|     CSI-NewYork|    USA|
    +---+------+----------------+-------+
    +---------------+-----------+
    |      tableName|isTemporary|
    +---------------+-----------+
    |        courses|      false|
    |          data2|      false|
    |          data3|      false|
    |           empl|      false|
    |          empl2|      false|
    |      employees|      false|
    |hdfsmoviespart1|      false|
    |hdfsmoviespart2|      false|
    |         movies|      false|
    |      permtable|      false|
    |     permtable1|      false|
    |      sample_07|      false|
    |      sample_08|      false|
    |          stuff|      false|
    +---------------+-----------+

That was much less of the code! Wasn't it?

## Solution 3. LOAD DATA INPATH command ##

Let's suppose you've got a file on your local subsystem (in '/tmp' folder, I don't wanna mess with priviledges) and you want to move it to the HIVE permanent table. 
Use the LOAD DATA [LOCAL] INPATH 'hdfs or local file path' statement. When LOCAL word is used, local file system is referenced. If LOCAL word is ommited, HDFS is searched.

    import org.apache.spark.sql.hive.HiveContext;

    var hiveContext = new HiveContext(sc);

    hiveContext.sql(" CREATE EXTERNAL TABLE IF NOT EXISTS HDFSMoviesPart3 (id string, genre string, name string, country string) "+
                " ROW FORMAT DELIMITED "+
                " FIELDS TERMINATED BY ','"    
    );

    hiveContext.sql(" LOAD DATA LOCAL INPATH '/tmp/movies.txt' OVERWRITE INTO TABLE HDFSMoviesPart3 ");

    hiveContext.sql(" SELECT * from HDFSMoviesPart3 ").show();

    hiveContext.sql(" SHOW TABLES ").show();


Output from Zeppelin run:

    import org.apache.spark.sql.hive.HiveContext
    hiveContext: org.apache.spark.sql.hive.HiveContext = org.apache.spark.sql.hive.HiveContext@26c4a185
    res21: org.apache.spark.sql.DataFrame = [result: string]
    res22: org.apache.spark.sql.DataFrame = [result: string]
    +---+------+----------------+-------+
    | id| genre|            name|country|
    +---+------+----------------+-------+
    |  1| scifi|          Matrix|    USA|
    |  2| scifi| Matrix-Reloaded|    USA|
    |  3|comedy|TheBigBangTheory|    USA|
    |  4|comedy|          MrBean|    GBR|
    |  5|comedy| CrocodileDundee|    AUS|
    |  6|comedy|         Pelisky|    CZE|
    |  7| crime|     CSI-NewYork|    USA|
    +---+------+----------------+-------+
    +---------------+-----------+
    |      tableName|isTemporary|
    +---------------+-----------+
    |        courses|      false|
    |          data2|      false|
    |          data3|      false|
    |           empl|      false|
    |          empl2|      false|
    |      employees|      false|
    |hdfsmoviespart1|      false|
    |hdfsmoviespart2|      false|
    |hdfsmoviespart3|      false|
    |         movies|      false|
    |      permtable|      false|
    |     permtable1|      false|
    |      sample_07|      false|
    |      sample_08|      false|
    |          stuff|      false|
    +---------------+-----------+

Okay, these are the most used ways of publishing HDFS to HIVE....Something like that I can expect at the ONLINE exam.
I'm starting to be nervous...

regards

T.

