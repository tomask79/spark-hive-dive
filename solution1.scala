import org.apache.spark.sql.hive.HiveContext;

var hiveContext = new HiveContext(sc);

case class Movie(id: Int, genre: String, name: String, country: String);

val fileRDD = sc.textFile("/tests/movies.txt");

val movieDF = fileRDD.map(line=>new Movie(line.split(",")(0).toInt, line.split(",")(1), line.split(",")(2), line.split(",")(3))).toDF();

movieDF.write.mode("overwrite").saveAsTable("HDFSMoviesPart1");

movieDF.show();

hiveContext.sql("SHOW TABLES").show();