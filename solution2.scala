import org.apache.spark.sql.hive.HiveContext;

var hiveContext = new HiveContext(sc);

hiveContext.sql("CREATE EXTERNAL TABLE IF NOT EXISTS HDFSMoviesPart2 (id int, genre string, name string, country string) "+
                " ROW FORMAT DELIMITED "+
                " FIELDS TERMINATED BY ','"+
                " LOCATION '/tests/dir'"
);

hiveContext.sql("SELECT * from HDFSMoviesPart2").show();

hiveContext.sql("SHOW TABLES").show();