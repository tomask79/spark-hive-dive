import org.apache.spark.sql.hive.HiveContext;

var hiveContext = new HiveContext(sc);

hiveContext.sql(" CREATE EXTERNAL TABLE IF NOT EXISTS HDFSMoviesPart3 (id string, genre string, name string, country string) "+
                " ROW FORMAT DELIMITED "+
                " FIELDS TERMINATED BY ','"    
);

hiveContext.sql(" LOAD DATA LOCAL INPATH '/tmp/movies.txt' OVERWRITE INTO TABLE HDFSMoviesPart3 ");

hiveContext.sql(" SELECT * from HDFSMoviesPart3 ").show();

hiveContext.sql(" SHOW TABLES ").show();